

PROGRAM _INIT
	ACS380_m;
	ACS380_e;

END_PROGRAM


PROGRAM _CYCLIC
	
	CASE step OF
		0:
			IF MBMOpen_0.ident <> 0 THEN
				step := step + 1;
			ELSE
				step := step + 2;
			END_IF
			
		1:
			MBMClose_0.enable := TRUE;
			MBMClose_0.ident := MBMOpen_0.ident;
			MBMClose_0();
			IF MBMClose_0.status = 0 THEN
				step := step + 1;
			END_IF
		2:
			MBMOpen_0.enable		:= TRUE;
			MBMOpen_0.pDevice	:= ADR('IF6.ST1.IF1');  (* Device description string *)
			MBMOpen_0.pMode		:= ADR('/PHY=RS485 /PA=E /DB=8 /SB=1 /BD=19200');  (* Mode description string *)
			MBMOpen_0.pConfig	:= ADR('datamod');  (* Data object name *)
			MBMOpen_0.timeout	:= 2000;  (* Timeout in milliseconds (the value must be a multiple of 10 and >250ms) *)
			MBMOpen_0.ascii		:= 0;  (* 0 = RTU / 1 = ASCII *)
			MBMOpen_0();
		
			IF MBMOpen_0.status = 0 THEN
				step := step + 1;
			END_IF
			
		3:
			(*Modbus communication*)	
			MBMaster_0.enable	:= TRUE;
			MBMaster_0.ident		:= MBMOpen_0.ident;
			MBMaster_0();
			IF MBMaster_0.status <> 16#FFFF AND MBMaster_0.status <> 0 THEN
			//	step := step + 1;
			END_IF
	END_CASE;
	
	ACS380_e.StatusWord := TRUE;
	ACS380_e.Reference1 := TRUE;
	ACS380_e.ControlWord := TRUE;
	
	ACS380_m.Reference1 := REAL_TO_INT(20000*frequency/50);
END_PROGRAM
