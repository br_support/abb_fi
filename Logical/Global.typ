
TYPE
	ACS380_mapping_typ : 	STRUCT  (* * -> Compulsory*)
		ControlWord : UINT; (*CMDD_Output **)
		Reference1 : INT; (*LFRD_Output **)
		StatusWord : UINT; (*ETAD_Input **)
	END_STRUCT;
	ACS380_events_typ : 	STRUCT 
		ControlWord : BOOL; (*CMDD_Output **)
		Reference1 : BOOL;
		StatusWord : BOOL; (*ETAD_Input **)
	END_STRUCT;
END_TYPE
