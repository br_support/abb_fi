# ABB_FI

V následujícím textu je popsáno jak rozchodit ABB menič ACS380 na sběrnici Modbus RTU.

## Zapojení

Na straně PLC byla použita RS485 karta X20CS1030 s aktivním ukončovacím odporem.

Do měniče bylo nutné přidat komunikační kartu BMIO-01.

Propojení pinů:

DATA	B+
DATA\	A-
GND	    BGND


## Parametry měniče

Nejprve je nutné nastavit parametry měniče:

parametr 96.06 => 34560 provede reset do továrního nastavení
parametr 58.01 => 1 aktivuje Modbus RTU
parametr 58.03 => 1 nastaví node number 1
parametr 99.06 => 3.2 nastaví jmenovitý proud motoru 3.2A
parametr 99.07 => 230.0 nastaví jmenovité napětí motoru 230V
parametr 99.09 => 1395 nastaví jmenovitou rychlost motoru 1395rpm 
parametr 99.10 => 0.75 nastaví jmenovitý výkon motoru 0.75kW
parametr 99.11 => 0.80 nastaví cos(fi)
parametr 20.01 => 14 bude brát příkazy z Modbusu
parametr 28.11 => 8 bude brát požadovanou frekvenci z Modbusu

## AS Projekt

Ke komunikaci s měničem slouží:

ControlWord...řídící slovo
StatusWord...stavové slovo
frequency...požadovaná frekvence

## Stavový diagram

Aby se motor roztočil, je nutné měnič pomocí řídícího slova dostat do stavu OPERATION. 
